from django.contrib import admin
from .models import Hat, LocationVO


@admin.register(Hat)
class HatAdmin(admin.ModelAdmin):
    list_display = (
        "style_name",
        "id",
    )


@admin.register(LocationVO)
class LocationVOAdmin(admin.ModelAdmin):
    list_display = (
        "closet_name",
        "id",
    )
