# Wardrobify

Team:

* Wayne Basile - Hats
* Michael Parnapy - Shoes

## Design

The structure of this project follows the principles of Domain Driven Design by incorporating Microservices architecture and Bounded Contexts (each microservice). It also identifies value objects and integrates Inter-Processs communication to poll for the value objects between different microservices, which enables it to maintain good Data Stewardship.

## Shoes microservice

The shoes microservice involves a self contained microservice that interacts with the other applicable services in the domain through polling. The project works through creating a RESTful API to get a list of shoes, create a new shoe, and delete a shoe within its location in the wardrobe. React is used to display the data in an eye appealing manner.

## Hats microservice and Design

The hats microservice follows the principles derived from Domain Driven Design. It runs in its own container, and it is its own Bounded Context. Because the hats microservice depends on the location model in the wardrobe microservice, incorporating polling allows location value objects to be created for the hats microservice while still maintaining the independency of each microservices model and data, which is an integral part of Data Sovereignty.
