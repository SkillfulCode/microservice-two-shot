import React, {useEffect, useState} from "react";

function ShoesForm() {

    const [bin, setBin] = useState('');
    const [manufacturer, setManufacturer] = useState('');
    const [modelName, setModelName] = useState('');
    const [color, setColor] = useState('');
    const [pictureUrl, setPictureUrl] = useState('');
    const [bins, setBins] = useState([]);
    const [hasSignedUp, setHasSignedUp] = useState(false);

    const fetchData = async () => {
        const url = 'http://localhost:8100/api/bins/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setBins(data.bins);
        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};
        data.bin = bin;
        data.manufacturer = manufacturer;
        data.model_name = modelName;
        data.color = color;
        data.picture_url = pictureUrl;

        const shoeUrl = 'http://localhost:8080/api/shoes/';
        const fetchOptions = {
            method: 'POST',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const shoeResponse = await fetch(shoeUrl, fetchOptions);
        if (shoeResponse.ok) {
            setBin('');
            setManufacturer('');
            setModelName('');
            setColor('');
            setPictureUrl('');
            setHasSignedUp(true);
        }
    }

    const handleBinChange = (event) => {
        const value = event.target.value;
        setBin(value);
    }

    const handleManufacturerChange = (event) => {
        const value = event.target.value;
        setManufacturer(value);
    }

    const handleModelNameChange = (event) => {
        const value = event.target.value;
        setModelName(value);
    }

    const handleColorChange = (event) => {
        const value = event.target.value;
        setColor(value);
    }

    const handlePictureUrlChange = (event) => {
        const value = event.target.value;
        setPictureUrl(value);
    }

    let spinnerClasses = 'd-flex justify-content-center mb-3';
    let dropdownClasses = 'form-select d-none';
    if (bins.length > 0) {
        spinnerClasses = 'd-flex justify-content-center mb-3 d-none';
        dropdownClasses = 'form-select';
    }

    let messageClasses='alert alert-success d-none mb-0';
    let formClasses = '';
    if (hasSignedUp) {
        messageClasses = 'alert alert-success mb-0';
        formClasses = 'd-none';
    }

    return(
        <div className="my-5 container">
            <div className="row">
                <div className="col">
                    <div className="card shadow">
                        <div className="card-body">
                            <form className={formClasses} onSubmit={handleSubmit} id="create-shoe-form">
                                <h1 className="card-title">Shoes for you</h1>
                                <p className="mb-3">
                                    Please submit your shoe information.
                                </p>
                                <div className={spinnerClasses} id="loading-location-spinner">
                                    <div className="spinner-grow text-secondary" role="status">
                                        <span className="visually-hidden">Loading Bin...</span>
                                    </div>
                                </div>
                                <div className="mb-3">
                                    <select onChange={handleBinChange} name="bin" id="bin" className={dropdownClasses} required>
                                        <option value="">Choose a shoe bin</option>
                                        {bins.map(bin => {
                                            return (
                                                <option key={bin.href} value={bin.href}>{bin.closet_name}</option>
                                            )
                                        })}
                                    </select>
                                </div>
                                <p className="mb-3">
                                    Please submit your shoe information.
                                </p>
                                <div className="row">
                                    <div className="col">
                                        <div className="form-floating mb-3">
                                            <input onChange={handleManufacturerChange} required placeholder="Your shoe manufacturer" type="text" id="manufacturer" name="manufacturer" className="form-control" />
                                            <label htmlFor="name">Your shoe manufacturer</label>
                                        </div>
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col">
                                        <div className="form-floating mb-3">
                                            <input onChange={handleModelNameChange} required placeholder="Your shoe model name" type="text" id="model_name" name="model_name" className="form-control" />
                                            <label htmlFor="name">Your shoe model name</label>
                                        </div>
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col">
                                        <div className="form-floating mb-3">
                                            <input onChange={handleColorChange} required placeholder="Your shoe color" type="text" id="color" name="color" className="form-control" />
                                            <label htmlFor="color">Your shoe color</label>
                                        </div>
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col">
                                        <div className="form-floating mb-3">
                                            <input onChange={handlePictureUrlChange} required placeholder="Your shoe picture web link" type="text" id="picture_url" name="picture_url" className="form-control" />
                                            <label htmlFor="name">Your shoe picture web link</label>
                                        </div>
                                    </div>
                                </div>
                                <button className="btn btn-lg btn-primary">Save Shoes</button>
                            </form>
                            <div className={messageClasses} id="success-messaga">
                                Congratulations! You've got shoes!
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default ShoesForm;
