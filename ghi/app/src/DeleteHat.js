import React, {useEffect, useState } from 'react';

function DeleteHatForm() {

  const [hat, setHat] = useState('');
  const [hats, setHats] = useState([]);
  const [hasDeletedHat, setHasDeletedHat] = useState(false);

  const fetchData = async () => {
    const url = 'http://localhost:8090/api/hats/';
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setHats(data.hats);
    }
  }

  useEffect(() => {
    fetchData();
  }, []);

  const handleSubmit = async (event) => {
    event.preventDefault();
    const data = {};
    data.hat = hat;

    const hatUrl = `http://localhost:8090${data.hat}`;
    const fetchOptions = {
      method: 'delete',
      headers: {
        'Content-Type': 'application/json',
      },
    };

    const hatResponse = await fetch(hatUrl, fetchOptions);
    if (hatResponse.ok) {
      setHat('');
      setHasDeletedHat(true);
    }
  }

  const handleDeleteHat = (event) => {
    const value = event.target.value;
    setHat(value);
  }

  let spinnerClasses = 'd-flex justify-content-center mb-3';
  let dropdownClasses = 'form-select d-none';
  if (hats.length > 0) {
    spinnerClasses = 'd-flex justify-content-center mb-3 d-none';
    dropdownClasses = 'form-select';
  }

  let messageClasses = 'alert alert-success d-none mb-0';
  let formClasses = '';
  if (hasDeletedHat) {
    messageClasses = 'alert alert-success mb-0';
    formClasses = 'd-none';
  }

  return (
    <div className="my-5 container">
      <div className="row">
        <div className="col">
          <div className="card shadow">
            <div className="card-body">
              <form className={formClasses} onSubmit={handleSubmit} id="create-hat-form">
                <h1 className="card-title">It's Hat Time!</h1>
                <p className="mb-3">
                  Please choose which hat you'd like to delete.
                </p>
                <div className={spinnerClasses} id="loading-location-spinner">
                  <div className="spinner-grow text-secondary" role="status">
                    <span className="visually-hidden">Loading...</span>
                  </div>
                </div>
                <div className="mb-3">
                  <select onChange={handleDeleteHat} name="hat" id="hat" className={dropdownClasses} required>
                    <option value="">Choose a hat to delete</option>
                    {hats.map(hat => {
                      return (
                        <option key={hat.href} value={hat.href}>{hat.style_name}</option>
                      )
                    })}
                  </select>
                </div>
                <button className="btn btn-lg btn-primary">Delete a hat!</button>
              </form>
              <div className={messageClasses} id="success-message">
                You deleted a hat... but y tho? 😢
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default DeleteHatForm;
