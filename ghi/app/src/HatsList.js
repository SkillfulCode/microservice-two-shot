import React, {useEffect, useState } from 'react';


function HatsApp() {

  // render dynamically (not using index)
  const [hats, setHats] = useState([]);

  async function loadHats() {
    const response = await fetch('http://localhost:8090/api/hats/');
    if (response.ok) {
      const data = await response.json();
      setHats(data.hats);
    }
  }

  useEffect(() => {
    loadHats();
  }, []);

  // auto update hats !!
  async function handleDeleteHat(href) {
    const hatUrl = `http://localhost:8090${href}`;
    const fetchOptions = {
      method: 'delete',
      headers: {
        'Content-Type': 'application/json',
      },
    }
    const hatResponse = await fetch(hatUrl, fetchOptions);
    if (hatResponse.ok) {
      loadHats();
    }
  };

    return (
      <div className="container my-5">
        <table className="table">
          <thead>
            <tr className="bg-primary">
              <th>Style Name</th>
              <th>Fabric</th>
              <th>Color</th>
              <th>URL</th>
              <th>Locations</th>
              <th></th>
            </tr>
          </thead>
          <tbody>
            {hats && hats.map(hat => {
              return (
                <tr className="table-info" key={hat.href} value={hat.href}>
                  <td>{ hat.style_name }</td>
                  <td>{ hat.fabric }</td>
                  <td>{ hat.color }</td>
                  <td>{ hat.url }</td>
                  <td>{ hat.location }</td>
                  <td>
                    <button className="btn btn-sm btn-primary"
                    onClick={() => handleDeleteHat(hat.href)}>
                      Delete
                    </button>
                  </td>
                </tr>
              );
            })}
          </tbody>
      </table>
      </div>
    );
  }

  export default HatsApp;
