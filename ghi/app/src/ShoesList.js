function shoesApp(props) {
    return(
        <div className="container">
            <table className="table">
                <thead>
                    <tr className="bg-primary">
                        <th>Manufacturer</th>
                        <th>Model Name</th>
                        <th>Color</th>
                        <th>Picture</th>
                        <th>Bin</th>
                    </tr>
                </thead>
                <tbody>
                    {props.shoes && props.shoes.map(shoe => {
                        return (
                            <tr className="table-info" key={shoe.href}>
                                <td>{shoe.manufacturer}</td>
                                <td>{shoe.model_name}</td>
                                <td>{shoe.color}</td>
                                <td>{shoe.picture_url}</td>
                                <td>{shoe.bin}</td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </div>
    );
}

export default shoesApp;
