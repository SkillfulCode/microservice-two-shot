import React, {useEffect, useState} from 'react';

function ShoesDeleteForm() {

    const [shoe, setShoe] = useState('');
    const [shoes, setShoes] = useState([]);
    const [hasDeletedShoe, setHasDeletedShoe] = useState('');

    const fetchData = async () => {
        const url = 'http://localhost:8080/api/shoes/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setShoes(data.shoes);
        }
    }

    useEffect(() => {
        fetchData();
    },[]);

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};
        data.shoe = shoe;

        const shoeUrl = `http://localhost:8080${data.shoe}`;
        const fetchOptions = {
            method: 'delete',
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const shoeResponse = await fetch(shoeUrl, fetchOptions);
        if (shoeResponse.ok) {
            setShoe('')
            setHasDeletedShoe(true);
        }
    }

    const handleDeleteShoe = (event) => {
        const value = event.target.value;
        setShoe(value)
    }

    let spinnerClasses = 'd-flex justify-content-center mb-3';
    let dropdownClasses = 'form-select d-none';
    if (shoes.length > 0) {
        spinnerClasses = 'd-flex justify-content-center mb-3 d-none';
        dropdownClasses = 'form-select';
    }

    let messageClasses = 'alert alert-success d-none mb-0';
    let formClasses = '';
    if (hasDeletedShoe) {
        messageClasses = 'alert alert-success mb-0';
        formClasses = 'd-none';
    }

    return(
        <div className='my-5 container'>
            <div className='row'>
                <div className='col'>
                    <div className='card shadow'>
                        <div className='card-body'>
                            <form className={formClasses} onSubmit={handleSubmit} id='create-shoe-form'>
                                <h1 className='card-title'>Shoes Information</h1>
                                <p className='mb-3'>
                                    Please choose which shoes you would like to delete.
                                </p>
                                <div className={spinnerClasses} id='loading-bin-spinner'>
                                    <div className='spinner-grow text-secondary' role='status'>
                                        <span className='visually-hidden'>Loading bin...</span>
                                    </div>
                                </div>
                                <div className='mb-3'>
                                    <select onChange={handleDeleteShoe} name='shoe' id='shoe' className={dropdownClasses} required>
                                        <option value=''>Choose which shoes to delete</option>
                                        {shoes.map(shoe => {
                                            return(
                                                <option key={shoe.href} value={shoe.href}>{shoe.model_name}</option>
                                            )
                                        })}
                                    </select>
                                </div>
                                <button className='btn btn-lg btn-primary'>Delete these shoes</button>
                            </form>
                            <div className={messageClasses} id='success-message'>
                                The selected shoes have been deleted.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default ShoesDeleteForm;
