import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import HatsList from './HatsList';
import HatForm from './HatForm';
import DeleteHatForm from './DeleteHat';
import ShoesList from './ShoesList';
import ShoesForm from './ShoesForm';
import ShoesDeleteForm from './ShoesDeleteForm';

function App(props) {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />

          <Route path="hats">
            <Route index element={<HatsList />} />
            <Route path="new" element={<HatForm />} />
            <Route path="delete" element={<DeleteHatForm />} />
          </Route>

          <Route path='shoes'>
            <Route index element={<ShoesList shoes={props.shoes}/>}/>
            <Route path='new' element={<ShoesForm/>}/>
            <Route path='delete' element={<ShoesDeleteForm/>}/>
          </Route>

        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
